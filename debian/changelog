endlessh (1.1-5) unstable; urgency=medium

  [ Thierry B ]
  * d/endlessh.init: Background the server process.
    Closes: #972388

  [ nicoo ]
  * d/README: Explain socket activation and privileged ports
    Closes: #970512
  * Update nicoo's name
  * d/patches: Fixup DEP-3 metadata format

 -- nicoo <nicoo@debian.org>  Mon, 02 Nov 2020 14:13:46 +0100

endlessh (1.1-4) unstable; urgency=low

  * Replace Makefile patch with upstream's
  * debian/control: Set Rules-Requires-Root to 'no'
  * debian/tests: Add basic debci test

 -- nicoo <nicoo@debian.org>  Tue, 31 Mar 2020 19:38:12 +0200

endlessh (1.1-3) unstable; urgency=medium

  [ nicoo ]
  * CI: Remove the configuration file.
    Instead, we use 'salsa-ci.yml@nicoo/conventions' as the config path.

  [ Helmut Grohne ]
  * debian/rules: Let dh_auto_make pass cross-compilation tools to make
    Closes: #950526

 -- nicoo <nicoo@debian.org>  Thu, 05 Mar 2020 03:34:00 +0100

endlessh (1.1-2) unstable; urgency=medium

  * debian/patches:
    + Fix fuzz in 0001-Fix-binary-path-in-endlessh.service
    + Patch Makefile to honour CPPFLAGS

  * debian/rules
    + Correctly use recursive make
    + Honour buildflags (incl. hardening)
      Removed corresponding Lintian override

  * Add CI using salsa-ci-team's pipeline
  * Update authorship metadata

 -- nicoo <nicoo@debian.org>  Sun, 16 Feb 2020 12:56:39 +0100

endlessh (1.1-1) unstable; urgency=medium

  * New upstream version 1.1
    + Drop obsolete patches

  * debian/control
    + Fixup the Section (net, not devel...)
    + Link to the packaging repository on Salsa

  * Add upstream's signing key
  * debian/watch: Use git mode, check signature on tags

  * Declare compliance with policy v4.5.0.
    No change required.
  * Lintian: Override false positive (hardening-no-fortify-functions)

 -- nicoo <nicoo@debian.org>  Sat, 01 Feb 2020 05:29:51 +0100

endlessh (1.0-1) unstable; urgency=medium

  * Initial release (Closes: #933846)

 -- nicoo <nicoo@debian.org>  Sun, 04 Aug 2019 23:33:35 +0200
